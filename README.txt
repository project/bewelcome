RESOURCES

For the official home of the project and git repository for the install profile
visit: http://drupal.org/project/bewelcome

Note that this project contains only the basic framework: the drush
make file and the install profile.


INSTALLATION

You need drush and drush make. Normally you can fetch it from
http://drupal.org/project/drush

On Ubuntu 10.10 and up you can use apt-get (not on 10.04 and below).
$ sudo apt-get install drush

After you have installed drush you can use it to fetch drush_make:

$ drush dl drush_make

And set up drush make.

$ mkdir -p ~/.drush
$ mv drush_make ~/.drush


Now you can make the platform.  This will build the Drupal setup, it
will fetch the latest Drupal core, contrib modules and the custom
modules and our theme living in another git repository.

$ drush make bewelcome.make ../bewelcome-build
