; $Id$
;
; BeWelcome Drupal makefile
; -------------------------


core = 7.x

; API version
; ------------
; Every makefile needs to declare it's Drush Make API version. This version of
; drush make uses API version `2`.

api = 2


projects[drupal][type] = core


; Contrib modules

; These modules don't have official D7 releases yet:
projects[boxes][subdir] = "contrib"
projects[boxes][version] = 1.0-beta2

projects[context][subdir] = "contrib"
projects[context][version] = 3.0-alpha3

projects[profile_privacy][subdir] = "contrib"
projects[profile_privacy][version] = 1.x-dev

; Official releases, we probably also want to stick to specific versions

projects[admin_menu][subdir] = "contrib"
projects[advanced_help][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[date][subdir] = "contrib"
projects[diff][subdir] = "contrib"
projects[ds][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[field_group][subdir] = "contrib"
projects[l10n_client][subdir] = "contrib"
projects[link][subdir] = "contrib"

projects[og][subdir] = "contrib"
projects[openlayers][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[privatemsg][subdir]= "contrib" 
projects[profile2][subdir]= "contrib" 

projects[rpx][subdir]= "contrib" 
projects[token][subdir]= "contrib" 
projects[views][subdir]= "contrib" 
projects[views_bulk_operations][subdir]= "contrib" 
projects[webform][subdir]= "contrib" 

;Development modules
projects[devel][subdir]= "contrib" 
projects[masquerade][subdir]= "contrib" 
projects[migrate][subdir]= "contrib" 
projects[migrate_extras][subdir]= "contrib" 
projects[openidadmin][subdir]= "contrib" 


;Clone the BeWelcome Drupal theme from gitorious
projects[bewelcometheme][type] = theme
projects[bewelcometheme][download][type] = git
projects[bewelcometheme][download][url] = git://gitorious.org/bewelcome-theme/bewelcome-theme.git


; If you want to install a module into a sub-directory, you can use the
; `subdir` attribute.
;projects[admin_menu][subdir] = custom

; To apply a patch to a project, use the `patch` attribute and pass in the URL
; of the patch.
;projects[admin_menu][patch][] = "http://drupal.org/files/issues/admin_menu.long_.31.patch"
